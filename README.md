# Vacuum Pump Controller Pfeiffer TCP 350

EPICS module to provide communications and read/write data from/to Pfeiffer TCP350 vacuum pump controller

## Startup Examples

`iocsh -r vac_ctrl_tcp350,catania -c 'requireSnippet(vac_ctrl_tcp350_ethernet.cmd, "DEVICENAME=LNS-LEBT-010:VAC-VEPT-02100, IPADDR=10.4.0.213, PORT=4002)'`

If ran as proper ioc service:
```
epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VEPT-02100")
epicsEnvSet(IPADDR, "10.10.1.21")
epicsEnvSet(PORT, "4002")
require vac_ctrl_tcp350, 2.0.0-catania
< ${REQUIRE_vac_ctrl_tcp350_PATH}/startup/vac_ctrl_tcp350_ethernet.cmd
```
